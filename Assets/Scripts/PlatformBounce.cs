﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBounce : MonoBehaviour
{
	private static float _jumpForce = 15f;
	public static float JumpForce
	{
		get => _jumpForce;
		set => _jumpForce = value;
	}

	// Start is called before the first frame update
	void Start()
	{
		_jumpForce = 15f;
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.relativeVelocity.y <= 0f)
		{
			Rigidbody2D rb = collision.rigidbody;

			if (rb != null)
			{
				rb.AddForce(new Vector2(0, _jumpForce), ForceMode2D.Impulse);
			}
		}
	}
}
