﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class Countdown : MonoBehaviour
{
    public static Action activeEffect;
    private TextMeshProUGUI _countdownText;
    private int powerUpDuration =  5;
    private Coroutine _coroutine;

    void OnEnable()
    {
        SurpriseBox.onHit += StartCountdown;
        DeathZone.onDeath += StopCountdown;
    }

    // Start is called before the first frame update
    void Start()
    {
        _countdownText = gameObject.GetComponent<TextMeshProUGUI>();
        _countdownText.enabled = false;
    }

    IEnumerator CountdownNow(bool nature)
    {
        _countdownText.enabled = true;

        if(nature)
        {
            PlatformBounce.JumpForce = 25;
        }
        else
        {
            activeEffect?.Invoke(); // Calls PlayerController.cs
        }

        for (int i = powerUpDuration; i > 0; i--)
        {
            _countdownText.SetText("{0}", i);
            yield return new WaitForSeconds(1f);
        }

        if (nature)
        {
            PlatformBounce.JumpForce = 15;
        }
        else
        {
            activeEffect?.Invoke(); // Calls PlayerController.cs
        }

        _countdownText.enabled = false;
        _coroutine = null;
    }

    void StartCountdown(bool _bool)
    {
        if(_coroutine == null)
        {
            _coroutine = StartCoroutine(CountdownNow(_bool));
        }
    }

    void StopCountdown()
    {
        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
        }

        _countdownText.enabled = false;
    }

    void OnDisable()
    {
        SurpriseBox.onHit -= StartCountdown;
        DeathZone.onDeath -= StopCountdown;
    }
}
