﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CameraFollow : MonoBehaviour
{
    public Transform player;
    private float _velocity;
    private bool _playerAlive = true;

    [SerializeField] private float _smoothTime = 0.05f;
    private GameObject _gameOverCanvas;

    void OnEnable()
    {
        DeathZone.onDeath += PlayerStatus;
    }

    // Start is called before the first frame update
    void Start()
    {
        _gameOverCanvas = gameObject.GetComponentInChildren<Canvas>().gameObject;
        _gameOverCanvas.SetActive(false);
        _smoothTime = 0.05f;
    }

    void FixedUpdate()
    {
        if(_playerAlive)
        {
            if (player.position.y > transform.position.y)
            {
                float positionY = Mathf.SmoothDamp(transform.position.y, player.position.y, ref _velocity, _smoothTime);
                transform.position = new Vector3(transform.position.x, positionY, transform.position.z);
            }
        }
        else
        {
            _gameOverCanvas.SetActive(true);
            _gameOverCanvas.GetComponentsInChildren<TextMeshProUGUI>()[1].SetText("Your score:\n{0}", Score.TopScore);
            _gameOverCanvas.transform.SetParent(null);
            _smoothTime = 0.3f;
            float positionY = Mathf.SmoothDamp(transform.position.y, _gameOverCanvas.transform.position.y, ref _velocity, _smoothTime);
            transform.position = new Vector3(transform.position.x, positionY, transform.position.z);
        }      
    }

    void PlayerStatus()
    {
        _playerAlive = !_playerAlive;
    }

    void OnDisable()
    {
        DeathZone.onDeath -= PlayerStatus;
    }
}
