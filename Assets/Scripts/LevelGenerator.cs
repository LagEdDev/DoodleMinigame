﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public GameObject surprisePlatformPrefab;

    private int _initialSpawn = 15;
    private float _horizontalWidth;
    private float _supriseChance = 0.8f; //%20 percent chance (1 - 0.8 is 0.2)
    private bool _firstPlatformIsSpawned = false;

    private Vector3 _lastPlatformPos;
    private GameObject _surprisePlatform;

    void OnEnable()
    {
        DeathZone.onDisablePlatform += SpawnNewRandomPlatform;
    }

    // Start is called before the first frame update
    void Start()
    {
        Score.TopScore = 0;
        _surprisePlatform = Instantiate(surprisePlatformPrefab);
        _surprisePlatform.SetActive(false);
        _lastPlatformPos = gameObject.transform.position;
        _horizontalWidth = Camera.main.aspect * Camera.main.orthographicSize;

        for(int i = 0; i < _initialSpawn; i++)
        {
            GameObject platform = ObjectPooler.Instance.GetObjInPool();

            if(platform == null)
            {
                return;
            }

            if(_firstPlatformIsSpawned)
            {
                float spriteHalfWidth = platform.GetComponent<SpriteRenderer>().bounds.size.x / 2;
                float spriteHeight = platform.GetComponent<SpriteRenderer>().bounds.size.y;

                float xRand = Random.Range(-_horizontalWidth + spriteHalfWidth, _horizontalWidth - spriteHalfWidth);
                float yRand = _lastPlatformPos.y + Random.Range(spriteHeight * 2f, spriteHeight * 4f);

                platform.transform.position = new Vector3(xRand, yRand, 0f);
                _lastPlatformPos.y = platform.transform.position.y;
                platform.SetActive(true);
            }
            else
            {
                platform.transform.position = _lastPlatformPos;
                platform.SetActive(true);
                _firstPlatformIsSpawned = true;
            }          
        }
    }

    public void SpawnNewRandomPlatform()
    {
        GameObject platform;

        if (Random.value > _supriseChance && !_surprisePlatform.activeInHierarchy)
        {
            platform = _surprisePlatform;
        }
        else
        {
            platform = ObjectPooler.Instance.GetObjInPool();

            if (platform == null)
            {
                return;
            }
        }
        
        float spriteHalfWidth = platform.GetComponent<SpriteRenderer>().bounds.size.x / 2;
        float spriteHeight = platform.GetComponent<SpriteRenderer>().bounds.size.y;

        float xRand = Random.Range(-_horizontalWidth + spriteHalfWidth, _horizontalWidth - spriteHalfWidth);
        float yRand = _lastPlatformPos.y + Random.Range(spriteHeight * 2f, spriteHeight * 4f);

        platform.transform.position = new Vector3(xRand, yRand, 0f);
        _lastPlatformPos.y = platform.transform.position.y;
        platform.SetActive(true);
    }

    void OnDisable()
    {
        DeathZone.onDisablePlatform -= SpawnNewRandomPlatform;
    }
}
