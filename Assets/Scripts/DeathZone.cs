﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DeathZone : MonoBehaviour
{
    public static Action onDisablePlatform;
    public static Action onDeath;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<PlatformEffector2D>())
        {
            collision.gameObject.SetActive(false);
            onDisablePlatform?.Invoke(); //Calls LevelGenerator.cs
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>())
        {
            collision.GetComponent<PlayerController>().scoreText.enabled = false;
            collision.gameObject.SetActive(false);
            onDeath?.Invoke(); //Calls CameraFollow.cs and Countdowm.cs
        }
    }
}
