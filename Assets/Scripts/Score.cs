﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score
{
    private static float _topScore;
    public static float TopScore
    {
        get => _topScore;
        set => _topScore = value;
    }
}
