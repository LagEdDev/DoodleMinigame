﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurpriseBox : MonoBehaviour
{
    public static System.Action<bool> onHit;

    private SpriteRenderer _boxSprite;
    private bool _isGood = false;
    private int _randNumber;
    private Coroutine _coroutine;

    void OnEnable()
    {
        _boxSprite = gameObject.GetComponent<SpriteRenderer>();
        _isGood = (Random.value > 0.5f);

        if (_isGood)
        {
            _boxSprite.color = Color.cyan;
        }
        else
        {
            _boxSprite.color = Color.red;
        }

        _randNumber = Random.Range(1, 3);
        _coroutine = StartCoroutine(ChangeColor());
    }

    // Update is called once per frame
    void Update()
    {
        _randNumber = Random.Range(1, 3);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<PlayerController>())
        {
            onHit?.Invoke(_isGood); //Calls Countdown.cs
        }
    }

    IEnumerator ChangeColor()
    {
        while(true)
        {
            yield return new WaitForSeconds(_randNumber);

            _isGood = !_isGood;

            if (_isGood)
            {
                _boxSprite.color = Color.cyan;
            }
            else
            {
                _boxSprite.color = Color.red;
            }
        }      
    }

    void OnDisable()
    {
        StopCoroutine(_coroutine);
    }
}
