﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public TextMeshProUGUI scoreText;

    [SerializeField] private float _movementSpeed = 13f;
    private float _moveInput;
    private float _spriteWidth;
    private float _horizontalWidth;
    private bool _invertedControl = false;

    private Rigidbody2D _playerRb;
    private SpriteRenderer _playerSprite;

    void OnEnable()
    {
        Countdown.activeEffect += InvertController;
    }

    // Start is called before the first frame update
    void Start()
    {
        _horizontalWidth = Camera.main.aspect * Camera.main.orthographicSize;
        _playerRb = GetComponent<Rigidbody2D>();
        _playerSprite = GetComponent<SpriteRenderer>();
        _spriteWidth = _playerSprite.GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        _moveInput = Input.GetAxis("Horizontal");
        
        if(_invertedControl)
        {
            _moveInput *= _movementSpeed;
            _moveInput *= -1;
        }
        else
        {
            _moveInput *= _movementSpeed;
        }

        CheckHorizontalBound();
        AddScore();
    }

    void FixedUpdate()
    {
        _playerRb.velocity = new Vector2(_moveInput, _playerRb.velocity.y);

       if(_moveInput < 0)
        {
            _playerSprite.flipX = true;
        }
       else if(_moveInput > 0)
        {
            _playerSprite.flipX = false;
        }
    }

    void CheckHorizontalBound()
    {
        if (gameObject.transform.position.x > _horizontalWidth + _spriteWidth)
        {
            gameObject.transform.position = new Vector3(-_horizontalWidth - _spriteWidth, gameObject.transform.position.y, transform.position.z);
        }
        else if (gameObject.transform.position.x < -_horizontalWidth - _spriteWidth)
        {
            gameObject.transform.position = new Vector3(_horizontalWidth + _spriteWidth, gameObject.transform.position.y, transform.position.z);
        }
    }

    void AddScore()
    {
        if(_playerRb.velocity.y > 0f && transform.position.y > Score.TopScore)
        {
            Score.TopScore = transform.position.y;
        }

        scoreText.SetText("Score: {0}", Mathf.Round(Score.TopScore));
    }

    void InvertController()
    {
        _invertedControl = !_invertedControl;
    }

    void OnDisable()
    {
        Countdown.activeEffect -= InvertController;
    }
}
